## Welcome!

Second homework of BSA .NET thread.

To run it .NET 5 is required.

To run the console app, open project root in Terminal/Command Line and then:

```
    cd CoolParking/CoolParking.App
    dotnet run
```

Then use instructions, that will appear in Terminal/Command line

<br />

**TIP: Transactions log file is located in project_root/CoolParking/CoolParking.App/bin/Debug/net5.0/Transactions.log**
