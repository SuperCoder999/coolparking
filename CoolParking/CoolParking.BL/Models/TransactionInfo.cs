﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string VehicleId { get; }
        public decimal Sum { get; }
        public DateTime Date { get; }

        public TransactionInfo(string vehicleId, decimal sum)
        {
            Sum = sum;
            VehicleId = vehicleId;
            Date = DateTime.Now;
        }

        public override string ToString()
        {
            return $"[{Date}] Withdrawn vehicle registered as {VehicleId}. Amount: {Sum}";
        }
    }
}
