﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public sealed class Parking
    {
        public List<Vehicle> vehicles = new List<Vehicle>();
        public int Capacity { get; internal set; } = Settings.parkingCapacity;
        public int FreePlaces { get; internal set; } = Settings.parkingCapacity;
        public decimal Balance { get; internal set; } = Settings.startParkingBalance;
    }
}
