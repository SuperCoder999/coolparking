namespace CoolParking.BL.Interfaces
{
    public interface IValidator<T>
    {
        void Validate(T obj);
    }
}
