using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Validators
{
    public sealed class VehicleIdValidator : IValidator<string>
    {
        public void Validate(string id)
        {
            if (!Settings.idValidationRegex.IsMatch(id.ToUpper()))
            {
                throw new ArgumentException(Settings.validationErrorMessages["invalidID"], nameof(id));
            }
        }
    }
}
