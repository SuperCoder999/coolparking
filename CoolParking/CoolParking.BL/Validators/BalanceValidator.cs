using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Validators
{
    public sealed class BalanceValidator : IValidator<decimal>
    {
        public void Validate(decimal balance)
        {
            if (balance < 0)
            {
                throw new ArgumentException(Settings.validationErrorMessages["invalidBalance"], nameof(balance));
            }
        }
    }
}
