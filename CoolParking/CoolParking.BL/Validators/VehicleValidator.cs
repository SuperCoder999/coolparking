using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Validators
{
    public sealed class VehicleValidator : IValidator<Vehicle>
    {
        private readonly BalanceValidator balanceValidator;
        private readonly VehicleIdValidator vehicleIdValidator;

        public VehicleValidator()
        {
            balanceValidator = new BalanceValidator();
            vehicleIdValidator = new VehicleIdValidator();
        }

        public void Validate(Vehicle obj)
        {
            vehicleIdValidator.Validate(obj.Id);
            balanceValidator.Validate(obj.Balance);
        }
    }
}
