﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Linq;
using System.Timers;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Validators;

namespace CoolParking.BL.Services
{
    public sealed class ParkingService : IParkingService
    {
        private static Parking _parking;
        private static List<TransactionInfo> _transactions;
        private static ITimerService _withdrawTimer;
        private static ITimerService _logTimer;
        private static ILogService _logger;
        private static int _instancesCount = 0;
        private static bool _hasSingleton = false;
        private readonly bool _localIsSingleton = false;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logger)
        {
            if (!_hasSingleton)
            {
                _withdrawTimer = withdrawTimer;
                _logTimer = logTimer;
                _logger = logger;
                _parking = new Parking();
                _transactions = new List<TransactionInfo>();
                _localIsSingleton = true;
                _hasSingleton = true;

                SetTimers();
            }
            else
            {
                bool differentWithdrawTimer = _withdrawTimer.Interval != withdrawTimer.Interval;
                bool differentLogTimer = _logTimer.Interval != logTimer.Interval;
                bool differentLogger = _logger.LogPath != logger.LogPath;
                bool illegalCreation = differentWithdrawTimer || differentLogTimer || differentLogger;

                if (illegalCreation)
                {
                    throw new InvalidOperationException(Settings.errorMessages["illegalSingletonCreation"]);
                }
            }

            _instancesCount++;
        }

        public void Dispose()
        {
            _instancesCount--;

            if (_localIsSingleton || _instancesCount == 0)
            {
                _withdrawTimer.Dispose();
                _logTimer.Dispose();

                _parking = null;
                _transactions = null;
                _withdrawTimer = null;
                _logTimer = null;

                _hasSingleton = false;
            }
        }

        public static ParkingService CreateWithDefaultSettings()
        {
            ILogService logger = new LogService(Settings.logfilePath);
            ITimerService withdrawTimer = new TimerService(Settings.paymentWithdrawDelaySecs);
            ITimerService logTimer = new TimerService(Settings.transactionLoggingDelaySecs);

            return new ParkingService(withdrawTimer, logTimer, logger);
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.FreePlaces;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_parking.vehicles);
        }

        public Vehicle GetOneVehicle(string id)
        {
            return _parking.vehicles.Find(v => v.Id == id);
        }

        public void AddVehicle(Vehicle data)
        {
            if (_parking.FreePlaces == 0)
            {
                throw new InvalidOperationException(Settings.errorMessages["noPlaces"]);
            }

            Vehicle checking = _parking.vehicles.Find(v => v.Id == data.Id);

            if (checking != null)
            {
                throw new ArgumentException(Settings.errorMessages["existingVehicleId"], nameof(data));
            }

            _parking.vehicles.Add(data);
            _parking.FreePlaces -= 1;
        }

        public void RemoveVehicle(string id)
        {
            int index = _parking.vehicles.FindIndex(v => v.Id == id);

            if (index < 0)
            {
                throw new ArgumentException(Settings.errorMessages["vehicleNotFound"], nameof(id));
            }

            if (_parking.vehicles[index].Balance < 0)
            {
                throw new InvalidOperationException(Settings.errorMessages["cantRemoveNoMoney"]);
            }

            _parking.vehicles.RemoveAt(index);
            _parking.FreePlaces += 1;
        }

        public void TopUpVehicle(string id, decimal sum)
        {
            new BalanceValidator().Validate(sum);
            Vehicle vehicle = _parking.vehicles.Find(v => v.Id == id);

            if (vehicle == null)
            {
                throw new ArgumentException(Settings.errorMessages["vehicleNotFound"], nameof(id));
            }

            vehicle.Balance += sum;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactions.ToArray();
        }

        public string ReadFromLog()
        {
            return _logger.Read();
        }

        public string GetAndStringifyLastParkingTransactions()
        {
            IEnumerable<string> transactionStrings = from trans in GetLastParkingTransactions()
                                                     select trans.ToString();

            return string.Join("\n", transactionStrings);
        }

        public decimal GetLastParkingTopUpSum()
        {
            IEnumerable<decimal> numbers = from trans in GetLastParkingTransactions()
                                           select trans.Sum;

            return numbers.Sum();
        }

        private void SetTimers()
        {
            _withdrawTimer.Start();
            _withdrawTimer.Elapsed += WithdrawVehicles;

            _logTimer.Start();
            _logTimer.Elapsed += LogTransactionsToFile;
        }

        private void CreateAndAddTransaction(string vehicleId, decimal sum)
        {
            TransactionInfo trans = new TransactionInfo(vehicleId, sum);
            _transactions.Add(trans);
        }

        private void WithdrawVehicles(object sender, ElapsedEventArgs args)
        {
            decimal total = 0;

            _parking.vehicles.ForEach(vehicle =>
            {
                decimal sum = CalculateWithdrawPrice(vehicle);
                vehicle.Balance -= sum;
                total += sum;
                CreateAndAddTransaction(vehicle.Id, sum);
            });

            _parking.Balance += total;
        }

        private decimal CalculateWithdrawPrice(Vehicle vehicle)
        {
            decimal normalPrice = Settings.vehiclePaymentRate[vehicle.Type];
            decimal withoutFine = Math.Max(Math.Min(normalPrice, vehicle.Balance), 0);
            decimal withFine = -Math.Max(Math.Min(vehicle.Balance - normalPrice, 0), -normalPrice) * Settings.fineCoefficient;
            decimal totalPrice = withoutFine + withFine;

            return totalPrice;
        }

        private void LogTransactionsToFile(object sender, ElapsedEventArgs args)
        {
            string toWrite = string.Join("\n", _transactions.ConvertAll<string>(v => v.ToString()));

            if (toWrite.Length > 0)
            {
                _logger.Write(toWrite);
                _transactions.Clear();
            }
            else
            {
                _logger.Write(Settings.noTransactionsStub);
            }
        }
    }
}
