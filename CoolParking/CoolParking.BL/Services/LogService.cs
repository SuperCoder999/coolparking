﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;
using System.IO;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public sealed class LogService : ILogService
    {
        public string LogPath { get; }

        public LogService(string path)
        {
            LogPath = path;
        }

        public void Write(string logInfo)
        {
            using (StreamWriter writer = File.AppendText(LogPath))
            {
                writer.WriteLine(logInfo);
            }
        }

        public string Read()
        {
            try
            {
                string content = File.ReadAllText(LogPath);
                return content;
            }
            catch (FileNotFoundException exc)
            {
                throw new InvalidOperationException(Settings.errorMessages["notExistingFile"], exc);
            }
        }
    }
}
