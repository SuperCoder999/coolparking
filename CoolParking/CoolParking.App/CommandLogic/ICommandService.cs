using System;

namespace CoolParking.App.CommandLogic
{
    public interface ICommandService : IDisposable
    {
        string GetCommandsStringInfo();
        void ExecuteCommand(byte index);
    }
}
