using System;
using System.Reflection;
using System.Collections.Generic;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.App.Commands;

namespace CoolParking.App.CommandLogic
{
    public sealed class CommandService : ICommandService
    {
        private readonly List<ICommand> commands;
        private readonly IParkingService parkingService = ParkingService.CreateWithDefaultSettings();

        public CommandService()
        {
            commands = FindCommands();
        }

        public void Dispose()
        {
            parkingService.Dispose();
        }

        private List<ICommand> FindCommands()
        {
            List<ICommand> result = new List<ICommand>();

            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type type in assembly.GetTypes())
                {
                    UseCommandAttribute attribute = type.GetCustomAttribute<UseCommandAttribute>();

                    if (attribute != null)
                    {
                        ICommand command = (ICommand)Activator.CreateInstance(type, parkingService);
                        result.Add(command);
                    }
                }
            }

            return result;
        }

        public void ExecuteCommand(byte index)
        {
            ICommand command = commands.Find(c =>
            {
                UseCommandAttribute metadata = c.GetMetadata();
                return metadata.Index == index;
            });

            if (command == null)
            {
                throw new InvalidOperationException(Settings.errorMessages["notFoundCommand"]);
            }

            command.Invoke();
        }

        public string GetCommandsStringInfo()
        {
            string result = "";
            List<ICommand> sortedCommands = new List<ICommand>(commands);

            sortedCommands.Sort((c1, c2) =>
            {
                UseCommandAttribute meta1 = c1.GetMetadata();
                UseCommandAttribute meta2 = c2.GetMetadata();

                return meta1.Index - meta2.Index;
            });

            foreach (ICommand command in sortedCommands)
            {
                result += command.GetStringInfo();
                result += "\n";
            }

            return result;
        }
    }
}
