using System;
using CoolParking.BL.Interfaces;

namespace CoolParking.App.Commands
{
    [UseCommand(3, "getPlacesInfo")]
    internal sealed class GetPlacesInfoCommand : BaseCommand<GetPlacesInfoCommand>
    {
        public GetPlacesInfoCommand(IParkingService parkingService) : base(parkingService) { }

        protected override void UnsafeInvoke()
        {
            int free = parkingService.GetFreePlaces();
            int capacity = parkingService.GetCapacity();

            Console.WriteLine($"Capacity: {capacity}");
            Console.WriteLine($"Free places: {free}");
            Console.WriteLine($"Busy places: {capacity - free}");
        }
    }
}
