using System;
using System.Collections.Generic;
using CoolParking.BL;
using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;
using CoolParking.App.CommandParameters;

namespace CoolParking.App.Commands
{
    internal abstract class BaseCommand<TDerivedClass> : ICommand where TDerivedClass : class
    {
        protected IParkingService parkingService;
        protected internal UseCommandAttribute attribute;
        protected List<UseParameterAttribute> parameters;
        private Dictionary<string, CommandParameterResult<dynamic>> parameterCache = new Dictionary<string, CommandParameterResult<dynamic>>();

        public BaseCommand(IParkingService parkingService)
        {
            attribute = GetUseCommandAttribute();
            parameters = FindParameters();
            this.parkingService = parkingService;
        }

        private List<UseParameterAttribute> FindParameters()
        {
            UseParameterAttribute checkAttribute = Utils.GetSingleAttribute<UseParameterAttribute, TDerivedClass>();

            if (checkAttribute == null)
            {
                return new List<UseParameterAttribute>();
            }

            return new List<UseParameterAttribute>(Utils.GetMultipleAttribute<UseParameterAttribute, TDerivedClass>());
        }

        private UseCommandAttribute GetUseCommandAttribute()
        {
            UseCommandAttribute result = Utils.GetSingleAttribute<UseCommandAttribute, TDerivedClass>();

            if (result == null)
            {
                throw new InvalidOperationException(Settings.errorMessages["noUseCommandAttribute"]);
            }

            return result;
        }

        protected CommandParameterResult<T> GetParameter<T>(string key, Func<string, T> converter = null)
        {
            if (parameterCache.ContainsKey(key))
            {
                return parameterCache[key] as CommandParameterResult<T>;
            }
            else
            {
                UseParameterAttribute param = parameters.Find(p => p.Key == key);

                if (param == null)
                {
                    throw new InvalidOperationException(Settings.errorMessages["notFoundParameter"]);
                }
                else
                {
                    CommandParameterResult<T> result = param.AskAndParse<T>(converter);
                    parameterCache[key] = result as CommandParameterResult<dynamic>;
                    return result;
                }
            }
        }

        public string GetStringInfo()
        {
            return $"{attribute.Index}. {attribute.Prompt}";
        }

        public virtual void Invoke()
        {
            try
            {
                parameterCache.Clear();
                UnsafeInvoke();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }

        public UseCommandAttribute GetMetadata()
        {
            return attribute;
        }

        protected abstract void UnsafeInvoke();
    }
}
