using System;
using CoolParking.BL.Validators;
using CoolParking.BL.Interfaces;
using CoolParking.App.CommandParameters;

namespace CoolParking.App.Commands
{
    [UseCommand(8, "removeVehicle")]
    [UseParameter("id", "vehicleId", typeof(VehicleIdValidator))]
    internal sealed class RemoveVehicleCommand : BaseCommand<RemoveVehicleCommand>
    {
        public RemoveVehicleCommand(IParkingService parkingService) : base(parkingService) { }

        protected override void UnsafeInvoke()
        {
            string id = GetParameter<string>("id").Value.ToUpper();
            parkingService.RemoveVehicle(id);

            Console.WriteLine($"Removed {id}");
        }
    }
}
