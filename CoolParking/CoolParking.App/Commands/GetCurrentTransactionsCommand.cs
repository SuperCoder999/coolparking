using System;
using CoolParking.BL.Interfaces;

namespace CoolParking.App.Commands
{
    [UseCommand(4, "getLastTransactions")]
    internal sealed class GetCurrentTransactionsCommand : BaseCommand<GetCurrentTransactionsCommand>
    {
        public GetCurrentTransactionsCommand(IParkingService parkingService) : base(parkingService) { }

        protected override void UnsafeInvoke()
        {
            Console.WriteLine(parkingService.GetAndStringifyLastParkingTransactions());
        }
    }
}
