using System;
using CoolParking.BL.Interfaces;

namespace CoolParking.App.Commands
{
    [UseCommand(2, "getLastTopUpSum")]
    internal sealed class GetLastTopUpSumCommand : BaseCommand<GetLastTopUpSumCommand>
    {
        public GetLastTopUpSumCommand(IParkingService parkingService) : base(parkingService) { }

        protected override void UnsafeInvoke()
        {
            Console.WriteLine(parkingService.GetLastParkingTopUpSum());
        }
    }
}
