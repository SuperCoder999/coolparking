using System;
using System.Linq;
using System.Collections.Generic;
using CoolParking.BL.Interfaces;

namespace CoolParking.App.Commands
{
    [UseCommand(6, "getRegisteredVehicles")]
    internal sealed class GetRegisteredVehiclesCommand : BaseCommand<GetRegisteredVehiclesCommand>
    {
        public GetRegisteredVehiclesCommand(IParkingService parkingService) : base(parkingService) { }

        protected override void UnsafeInvoke()
        {
            IEnumerable<string> strings = from vehicle in parkingService.GetVehicles()
                                          select vehicle.ToString();

            string result = string.Join("\n", strings);
            Console.WriteLine(result);
        }
    }
}
