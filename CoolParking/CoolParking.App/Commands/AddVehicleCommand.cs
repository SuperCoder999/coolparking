using System;
using CoolParking.BL.Models;
using CoolParking.BL.Validators;
using CoolParking.BL.Interfaces;
using CoolParking.App.CommandParameters;

namespace CoolParking.App.Commands
{
    [UseCommand(7, "addVehicle")]
    [UseParameter("type", "vehicleType")]
    [UseParameter("balance", "vehicleBalance", typeof(BalanceValidator))]
    internal sealed class AddVehicleCommand : BaseCommand<AddVehicleCommand>
    {
        public AddVehicleCommand(IParkingService parkingService) : base(parkingService) { }

        protected override void UnsafeInvoke()
        {
            string id = Vehicle.GenerateRandomRegistrationPlateNumber();
            decimal balance = GetParameter<decimal>("balance").Value;

            VehicleType type = GetParameter<VehicleType>("type", str => Vehicle.GetTypeByCleanedString(str)).Value;

            Vehicle vehicle = new Vehicle(id, type, balance);
            parkingService.AddVehicle(vehicle);

            Console.WriteLine($"Generated ID: {id}");
        }
    }
}
