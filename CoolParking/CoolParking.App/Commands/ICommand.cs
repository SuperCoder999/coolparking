namespace CoolParking.App.Commands
{
    internal interface ICommand
    {
        void Invoke();
        UseCommandAttribute GetMetadata();
        string GetStringInfo();
    }
}
