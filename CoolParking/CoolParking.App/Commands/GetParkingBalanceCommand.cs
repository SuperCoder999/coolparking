using System;
using CoolParking.BL.Interfaces;

namespace CoolParking.App.Commands
{
    [UseCommand(1, "getParkingBalance")]
    internal sealed class GetParkingBalanceCommand : BaseCommand<GetParkingBalanceCommand>
    {
        public GetParkingBalanceCommand(IParkingService parkingService) : base(parkingService) { }

        protected override void UnsafeInvoke()
        {
            Console.WriteLine(parkingService.GetBalance());
        }
    }
}
