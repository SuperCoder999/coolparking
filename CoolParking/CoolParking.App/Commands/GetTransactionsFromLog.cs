using System;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Validators;
using CoolParking.App.CommandParameters;

namespace CoolParking.App.Commands
{
    [UseCommand(5, "getLogTransactions")]
    [UseParameter("count", "logTransactionsCount", typeof(PositiveIntValidator), true)]
    internal sealed class GetTransactionsFromLogCommand : BaseCommand<GetTransactionsFromLogCommand>
    {
        public GetTransactionsFromLogCommand(IParkingService parkingService) : base(parkingService) { }

        protected override void UnsafeInvoke()
        {
            CommandParameterResult<int> countResult = GetParameter<int>("count");
            int count = 0;

            if (countResult.HasValue)
            {
                count = countResult.Value;

                if (count == 0)
                {
                    return;
                }

                string[] lines = parkingService.ReadFromLog().Split("\n");
                Console.WriteLine(string.Join("\n", lines.Skip(lines.Length - count - 1).Take(count)));
                return;
            }

            Console.WriteLine(parkingService.ReadFromLog());
        }
    }
}
