using System;
using CoolParking.BL.Validators;
using CoolParking.BL.Interfaces;
using CoolParking.App.CommandParameters;

namespace CoolParking.App.Commands
{
    [UseCommand(9, "topUpVehicle")]
    [UseParameter("id", "vehicleId", typeof(VehicleIdValidator))]
    [UseParameter("balance", "newVehicleBalance", typeof(BalanceValidator))]
    internal sealed class TopUpVehicleCommand : BaseCommand<TopUpVehicleCommand>
    {
        public TopUpVehicleCommand(IParkingService parkingService) : base(parkingService) { }

        protected override void UnsafeInvoke()
        {
            string id = GetParameter<string>("id").Value.ToUpper();
            decimal balance = GetParameter<decimal>("balance").Value;
            parkingService.TopUpVehicle(id, balance);

            Console.WriteLine($"Topped up {id}. New balance - {parkingService.GetOneVehicle(id).Balance}");
        }
    }
}
