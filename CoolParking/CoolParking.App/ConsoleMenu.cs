using System;
using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;
using CoolParking.App.CommandLogic;

namespace CoolParking.App
{
    public sealed class ConsoleMenu : IDisposable
    {
        private ICommandService commander;

        public ConsoleMenu()
        {
            commander = new CommandService();
        }

        public ConsoleMenu(ICommandService commandService)
        {
            commander = commandService;
        }

        public void Dispose()
        {
            commander.Dispose();
        }

        public void Run()
        {
            PrintAndAsk();
        }

        private void PrintAndAsk()
        {
            Console.Write(commander.GetCommandsStringInfo());
            Console.WriteLine($"(Enter command number or '{Settings.menuExitCommand}' to exit)");
            Console.Write(":");

            ProcessUserInput(Console.ReadLine());
        }

        private void ProcessUserInput(string input)
        {
            string cleanedInput = input.Trim();

            if (cleanedInput == Settings.menuExitCommand)
            {
                return;
            }

            try
            {
                Console.WriteLine();
                byte index = Convert.ToByte(cleanedInput);
                commander.ExecuteCommand(index);
            }
            catch (FormatException)
            {
                Console.WriteLine("Please, enter a valid number");
            }
            catch (InvalidOperationException exc)
            {
                Console.WriteLine(exc.Message);
            }
            catch (Exception exc)
            {
                Console.WriteLine($"Unknown error detected: {exc.Message}");
            }
            finally
            {
                Console.WriteLine();
                PrintAndAsk();
            }
        }
    }
}
