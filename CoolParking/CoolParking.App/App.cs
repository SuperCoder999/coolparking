﻿namespace CoolParking.App
{
    public sealed class App
    {
        public static void Main(string[] args)
        {
            using (ConsoleMenu app = new ConsoleMenu())
            {
                app.Run();
            }
        }
    }
}
